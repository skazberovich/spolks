#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
int main(int argc, char * argv[])
{
    int sock, listener;
    struct sockaddr_in addr;
    char buf[1024];
    int bytes_read;

    listener = socket(AF_INET, SOCK_STREAM, 0);

    if(listener < 0)
    {
        perror("socket");
        exit(1);
    }
    
    int16_t port_cmd = atoi(argv[2]);
    printf("\tPORT: %d\n", port_cmd);
    printf("\tIP  : %s\n", argv[1]);

    addr.sin_family = AF_INET;
    addr.sin_port = htons(port_cmd);
    addr.sin_addr.s_addr = inet_addr(argv[1]) ;

    if(bind(listener, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        perror("bind");
        exit(2);
    }

    listen(listener, 0);
    
    while(1)
    {
        sock = accept(listener, 0, 0);
        if(sock < 0)
        {
            printf("accept");
            return 0;
        }
        else 
        {
            printf("new connection\n");
        }
        

        switch(fork())
        {
        case -1:
            perror("fork");
            break;
            
        case 0:
            close(listener);
            while(1)
            {
                bytes_read = recv(sock, buf, 1024, 0);
                if(bytes_read <= 0) 
                    {
                        close(sock);
                        return 0;
                    }
                send(sock, buf, bytes_read, 0);
            }

            close(sock);
            return 0;

            
        default:
            close(sock);
        }
    }
    
    close(listener);

    return 0;
}