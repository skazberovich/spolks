#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

int main()
{
    int sock, server;
    struct sockaddr_in addr;
    char buf[1024];
    int bytes_read;

    server = socket(AF_INET, SOCK_STREAM, 0);
    if(server < 0)
    {
        perror("socket");
        exit(1);
    }
    
    addr.sin_family = AF_INET;
    addr.sin_port = htons(123);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
   // addr.sin_addr = inet_ntoa(172.31.29.217);
    if(bind(server, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        perror("bind");
        exit(2);
    }

    listen(server, 1);  //очередь
    
    while(1)
    {
        sock = accept(server, 0, 0);
        if(sock < 0)
        {
            perror("accept");
            exit(3);
        }

        while(1)
        {
            bytes_read = recv(sock, buf, 1024, 0);
            if(bytes_read <= 0) break;
            send(sock, buf, bytes_read, 0);
            printf(buf);
        }
        
    
        close(sock);
    }
    
    return 0;
}