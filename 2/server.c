#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <netdb.h>
#include <stdio.h>

#define BUF_SIZE 1024

void print_sended_size(long sended)
{
    printf("\rsended: %ld b", sended);
}

int main(int argc, char *argv[])
{
        int sockfd, client, port;
        long dpart, length;
        struct sockaddr_in server;
        FILE *file;
        char *command;
        char *filename;
        char buf[BUF_SIZE]; 
        long sended;

        if(argc > 1)
                printf("Server doesn't need the args\n");
        
       
        memset(buf, 0, BUF_SIZE);                         
        memset(&server, 0, sizeof(server));
        
        
        sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if(sockfd < 0) 
        {
                perror("error");
                exit(2);
        }
        
        server.sin_family = AF_INET;
        server.sin_addr.s_addr = INADDR_ANY;
        printf("port: ");
        scanf("%d", &port);
        server.sin_port = htons(port);
        
        if(bind(sockfd, (struct sockaddr*)&server, sizeof(server)) < 0) 
        {
                perror("error");
                exit(1);
        }
        
        printf("Server is working.\n");
        printf("Commands:\n");
        printf("\tsend <filename>n");
        printf("\texit - stop server\n");

        command = (char*)calloc(5, sizeof(char));
        filename = (char*)calloc(80, sizeof(char));

        while(1) 
        {
                scanf("%s", command);        
                
                if(!strcmp(command, "send"))
                {
                        scanf("%s", filename);
                        file = fopen(filename, "r");
                        if(file == NULL)
                        {
                                perror("Opening error");
                                continue;
                        }

                        listen(sockfd, 1);
                        client = accept(sockfd, NULL, NULL);
                        
                        if(client < 0) 
                        { 
                                perror("Accept error");
                                continue;
                        }
                        
                        send(client, (char*)basename(filename), sizeof(basename(filename)), 0);
                        recv(client, buf, sizeof(buf), 0);
                        dpart = atol(buf);
                        
                        sended = dpart;
                        fseek(file, dpart, SEEK_SET);
                        while(!feof(file))
                        {
                                length = fread(buf, 1, sizeof(buf), file);
                                if(length != 0)
                                        send(client, buf, length, 0);
                                sended += length;
                                print_sended_size(sended);
                        }
                        printf("\nDone.\n");
                        fclose(file);
                        close(client);
                }
                else if(!strcmp(command, "exit"))
                {
                        close(sockfd);
                        exit(0);
                }
                else
                        printf("bad command\n");
        }
        
        close(sockfd);
        return 0;
}

